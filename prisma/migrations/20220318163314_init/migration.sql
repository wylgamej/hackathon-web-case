-- CreateTable
CREATE TABLE "Test" (
    "id" SERIAL NOT NULL,
    "text" VARCHAR(512) NOT NULL,

    CONSTRAINT "Test_pkey" PRIMARY KEY ("id")
);
