-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "firstname" VARCHAR(512) NOT NULL,
    "lastname" VARCHAR(512) NOT NULL,
    "email" VARCHAR(512) NOT NULL,
    "phone" VARCHAR(512) NOT NULL,

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);
