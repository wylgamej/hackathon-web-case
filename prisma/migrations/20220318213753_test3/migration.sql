/*
  Warnings:

  - You are about to drop the column `avatarId` on the `User` table. All the data in the column will be lost.
  - You are about to drop the `Event` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Media` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_archivesEvents` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_guest` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_participian` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_planningEvents` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_authorId_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_posterId_fkey";

-- DropForeignKey
ALTER TABLE "User" DROP CONSTRAINT "User_avatarId_fkey";

-- DropForeignKey
ALTER TABLE "_archivesEvents" DROP CONSTRAINT "_archivesEvents_A_fkey";

-- DropForeignKey
ALTER TABLE "_archivesEvents" DROP CONSTRAINT "_archivesEvents_B_fkey";

-- DropForeignKey
ALTER TABLE "_guest" DROP CONSTRAINT "_guest_A_fkey";

-- DropForeignKey
ALTER TABLE "_guest" DROP CONSTRAINT "_guest_B_fkey";

-- DropForeignKey
ALTER TABLE "_participian" DROP CONSTRAINT "_participian_A_fkey";

-- DropForeignKey
ALTER TABLE "_participian" DROP CONSTRAINT "_participian_B_fkey";

-- DropForeignKey
ALTER TABLE "_planningEvents" DROP CONSTRAINT "_planningEvents_A_fkey";

-- DropForeignKey
ALTER TABLE "_planningEvents" DROP CONSTRAINT "_planningEvents_B_fkey";

-- AlterTable
ALTER TABLE "User" DROP COLUMN "avatarId";

-- DropTable
DROP TABLE "Event";

-- DropTable
DROP TABLE "Media";

-- DropTable
DROP TABLE "_archivesEvents";

-- DropTable
DROP TABLE "_guest";

-- DropTable
DROP TABLE "_participian";

-- DropTable
DROP TABLE "_planningEvents";
