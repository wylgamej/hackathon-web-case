-- CreateTable
CREATE TABLE "_helper" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_maybe" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_helper_AB_unique" ON "_helper"("A", "B");

-- CreateIndex
CREATE INDEX "_helper_B_index" ON "_helper"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_maybe_AB_unique" ON "_maybe"("A", "B");

-- CreateIndex
CREATE INDEX "_maybe_B_index" ON "_maybe"("B");

-- AddForeignKey
ALTER TABLE "_helper" ADD FOREIGN KEY ("A") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_helper" ADD FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_maybe" ADD FOREIGN KEY ("A") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_maybe" ADD FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
