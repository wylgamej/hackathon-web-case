import { BASE_URL, DEFAULT_IMAGE } from '../../config';
import { extendType, arg, inputObjectType, nonNull } from 'nexus';
import { Context } from '../../graphql/context';
import cuid from 'cuid';

export const EventMutation = extendType({
	type: 'Mutation',
	definition: (t) => {
		t.field('createOneEvent', {
			type: 'String',
			args: { data: nonNull(arg({ type: 'createOneEventInput' })) },
			resolve: async (_, { data }, ctx: Context) => {
				const event = await ctx.prisma.event.create({
					data: {
						title: data.title,
						description: data.description,
						shortDescription: data.shortDescription,
						organizer: data.organizer,
						type: data.type,
						place: data.place,
						telegramm: data.telegramm,
						date: data.date,
						format: data.format,
						seats: data.seats,
						author: {
							connect: {
								id: ctx.user.id,
							},
						},
						poster: {
							connectOrCreate: {
								where: {
									url: DEFAULT_IMAGE,
								},
								create: {
									url: DEFAULT_IMAGE,
									isApproved: true,
								},
							},
						},
					},
				});

				return event.id.toString();
			}
		});

		t.field('updateOneEvent', {
			type: 'String',
			args: { data: nonNull(arg({ type: 'updateOneEventInput' })) },
			resolve: async (_, { data }, ctx: Context) => {
				const eventData = await ctx.prisma.event.findUnique({
					where: {
						id: data.eventId,
					},
				});

				await ctx.prisma.event.update({
					where: {
						id: data.eventId,
					},
					data: {
						title: data.title !== null ? data.title : eventData.title,
						description: data.description !== null ? data.description : eventData.description,
						shortDescription: data.shortDescription !== null ? data.shortDescription : eventData.shortDescription,
						organizer: data.organizer !== null ? data.organizer : eventData.organizer,
						type: data.type !== null ? data.type : eventData.type,
						place: data.place !== null ? data.place : eventData.place,
						telegramm: data.telegramm !== null ? data.telegramm : eventData.telegramm,
						date: data.date !== null ? data.date : eventData.date,
						format: data.format !== null ? data.format : eventData.format,
						seats: data.seats !== null ? data.seats : eventData.seats,
					},
				});

				return 'Success!';
			},
		});

		t.field('deleteOneEvent', {
			type: 'String',
			args: { data: nonNull(arg({ type: 'deleteOneEventInput' })) },
			resolve: async (_, { data }, cxt: Context) => {
				await cxt.prisma.event.delete({
					where: {
						id: data.eventId,
					},
				});
				return 'Success';
			},
		});

		t.field('registerForEvent', {
			type: 'String',
			args: { data: nonNull(arg({ type: 'registerForEventInput' })) },
			resolve: async (_, { data }, ctx: Context) => {
				if (data.registerType == 'participians') {
					await ctx.prisma.event.update({
						where: {
							id: data.eventId,
						},
						data: {
							members: {
								connect: {
									id: ctx.user.id,
								},
							},
							participians: {
								connect: {
									id: ctx.user.id,
								},
							},
						},
					});

					if (data.decided == 'maybego') {
						await ctx.prisma.event.update({
							where: {
								id: data.eventId,
							},
							data: {
								maybe: {
									connect: {
										id: ctx.user.id,
									},
								},
							},
						});
					};

					return 'Success';
				} else if (data.registerType == 'guests') {
					await ctx.prisma.event.update({
						where: {
							id: data.eventId,
						},
						data: {
							members: {
								connect: {
									id: ctx.user.id,
								},
							},
							guests: {
								connect: {
									id: ctx.user.id,
								},
							},
						},
					});

					if (data.decided == 'maybego') {
						await ctx.prisma.event.update({
							where: {
								id: data.eventId,
							},
							data: {
								maybe: {
									connect: {
										id: ctx.user.id,
									},
								},
							},
						});
					};

					return 'Success';
				} else {
					return 'Failed, check registerType data.';
				};
			},
		});

		t.field('createInviteToEvent', {
			type: 'String',
			args: { data: nonNull(arg({ type: createInviteInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				const secret = cuid();
				const url = `${BASE_URL}/eventinvite?uid=${secret}`;

				await ctx.prisma.event.update({
					where: {
						id: data.eventId,
					},
					data: {
						inviteLink: url,
					},
				});

				return url;
			},
		});
	},
});

export const createOneEventInput = inputObjectType({
	name: 'createOneEventInput',
	definition(t) {
		t.nonNull.string('title');
		t.nonNull.string('organizer');
		t.nonNull.string('shortDescription');
		t.nonNull.string('description');
		t.boolean('repeat');
		t.nonNull.field('date', {
			type: 'DateTime'
		});
		t.string('place');
		t.field('format', {
			type: 'eventFormat'
		});
		t.field('type', {
			type: 'eventType'
		});
		t.string('telegramm');
		t.int('seats');
	},
});

export const updateOneEventInput = inputObjectType({
	name: 'updateOneEventInput',
	definition(t) {
		t.nonNull.int('eventId');
		t.string('title');
		t.string('organizer');
		t.string('description');
		t.string('shortDescription');
		t.field('date', {
			type: 'DateTime'
		});
		t.string('place');
		t.field('format', {
			type: 'eventFormat'
		});
		t.field('type', {
			type: 'eventType'
		});
		t.string('telegramm');
		t.int('seats');
	},
});

export const deleteOneEventInput = inputObjectType({
	name: 'deleteOneEventInput',
	definition(t) {
		t.nonNull.int('eventId');
	},
});

export const registerForEventInput = inputObjectType({
	name: 'registerForEventInput',
	definition(t) {
		t.nonNull.int('eventId');
		t.nonNull.field('decided', {
			type: 'registerDecided',
		});
		t.field('registerType', {
			type: 'registerType',
		});
	},
});

export const createInviteInput = inputObjectType({
	name: 'createInviteInput',
	definition(t) {
		t.nonNull.int('eventId');
	},
});