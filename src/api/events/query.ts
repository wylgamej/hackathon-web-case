import { arg, extendType, inputObjectType, nonNull } from 'nexus';
import { Context } from '../../graphql/context';

export const EventQuery = extendType({
	type: 'Query',
	definition: (t) => {
		t.crud.events({
			filtering: true,
			ordering: true,
		});
		t.field('getEvent', {
			type: 'Event',
			args: { data: nonNull(arg({ type: 'getEventInput' })) },
			resolve: async (_, { data }, ctx: Context) => {
				return ctx.prisma.event.findUnique({
					where: {
						id: data.eventId,
					},
				});
			},
		});
		t.list.field('getAllEvents', {
			type: 'Event',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.event.findMany();
			}
		});

		t.field('getEventStatistic', {
			type: 'EventStatistic',
			args: { data: nonNull(arg({ type: getEventStatisticInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				const users = await ctx.prisma.user.findMany();
				const event = await ctx.prisma.event.findUnique({
					where: {
						id: data.eventId,
					},
					include: {
						members: true,
						guests: true,
						participians: true,
						visitors: true,
						helpers: true,
						maybe: true,
					},
				});
				const before_event = await ctx.prisma.event.findMany({
					where: {
						title: event.title,
					},
					include: {
						members: true,
					}
				});

				return {
					"allMembers": event.members,
					"allMembersCount": event.members.length,
					"allMembersPercentToAllPeople": Math.round(event.members.length * 100 / users.length),
					"allMembersPercentToAllMembersLastEvent": event.members.length * 100 / before_event[before_event.length - 1].members.length,
					"willGo": event.members.filter((el) => !event.maybe.includes(el)),
					"willGoCount": (event.members.filter((el) => !event.maybe.includes(el))).length,
					"willGoPercentToAllMembers": Math.round((event.members.length - event.maybe.length) * 100 / event.members.length),
					"maybeGo": event.maybe,
					"maybeGoCount": event.maybe.length,
					"maybeGoPercentToAllMembers": Math.round(event.maybe.length * 100 / event.members.length),
					"helpers": event.helpers,
					"helpersCount": event.helpers.length,
					"helpersPercentToAllMembers": Math.round(event.helpers.length * 100 / event.members.length),
					"place": event.place,
					"date": event.date,
				};
			},
		});
	},
});

export const getEventInput = inputObjectType({
	name: 'getEventInput',
	definition(t) {
		t.nonNull.int('eventId');
	},
});

export const getEventStatisticInput = inputObjectType({
	name: 'getEventStatisticInput',
	definition(t) {
		t.nonNull.int('eventId');
	},
});
