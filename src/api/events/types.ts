import { enumType, objectType } from 'nexus';

export * from './query';
export * from './mutation';

export const Event = objectType({
	name: 'Event',
	definition(t) {
		t.model.id();
		t.model.inviteLink();
		t.model.title()
		t.model.description();
		t.model.shortDescription();
		t.model.organizer();
		t.model.date();
		t.model.place();
		t.model.format();
		t.model.type();
		t.model.telegramm();
		t.model.seats();
		t.model.poster();
		t.model.author();
		t.model.members();
		t.model.visitors();
		t.model.participians();
		t.model.guests();
		t.model.maybe();
	},
});

export const EventStatistic = objectType({
	name: 'EventStatistic',
	definition(t) {
		t.list.field('allMembers', {
			type: 'User',
		});
		t.int('allMembersCount');
		t.list.field('willGo', {
			type: 'User',
		});
		t.int('willGoCount');
		t.list.field('maybeGo', {
			type: 'User',
		});
		t.int('maybeGoCount');
		t.list.field('helpers', {
			type: 'User',
		});
		t.int('helpersCount');
		t.string('place');
		t.field('date', {
			type: 'DateTime',
		});
		t.int('allMembersPercentToAllPeople');
		t.int('allMembersPercentToAllMembersLastEvent');
		t.int('willGoPercentToAllMembers');
		t.int('maybeGoPercentToAllMembers');
	},
});

export const eventFormat = enumType({
	name: 'eventFormat',
	members: ['online', 'offline'],
});

export const eventType = enumType({
	name: 'eventType',
	members: ['local', 'command', 'general'],
});

export const registerDecided = enumType({
	name: 'registerDecided',
	members: ['willgo', 'maybego'],
});