/* eslint-disable sonarjs/no-duplicate-string */
/* eslint-disable sonarjs/no-identical-functions */
import { extendType, nonNull, arg, inputObjectType } from 'nexus';
import cuid from 'cuid';
import * as awsS3API from '../../integrations/aws/s3';
import { Context } from '../../graphql/context';
import { GraphQLError } from 'graphql';

export const MediaMutation = extendType({
	type: 'Mutation',
	definition: (t) => {
		t.field('putUserAvatar', {
			type: 'SignUrlResponse',
			args: { data: nonNull(arg({ type: getMediaDataInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				const fileName = await cuid();

				const result = await awsS3API.getSignedUrl({ fileName, type: "image/png" });

				await ctx.prisma.media.create({
					data: {
						users: {
							connect: {
								id: data.entityId,
							},
						},
						url: result.objectURL,
					},
				});

				return result;
			},
		});
		t.field('putEventPoster', {
			type: 'SignUrlResponse',
			args: { data: nonNull(arg({ type: getMediaDataInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				const fileName = await cuid();

				const result = await awsS3API.getSignedUrl({ fileName, type: "image/png" });

				await ctx.prisma.media.create({
					data: {
						events: {
							connect: {
								id: data.entityId,
							},
						},
						url: result.objectURL,
					},
				});

				return result;
			},
		});
		t.field('createMedia', {
			type: 'SignUrlResponse',
			args: { data: nonNull(arg({ type: createMediaInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				const fileName = await cuid();

				const result = await awsS3API.getSignedUrl({ fileName, type: "image/png" });

				const media = await ctx.prisma.media.create({
					data: {
						url: result.objectURL,
					},
				});

				if (data.entityType !== undefined) {
					switch (data.entityType) {
						case 'userAvatar': {
							await ctx.prisma.user.update({
								where: {
									id: data.entityId,
								},
								data: {
									avatar: {
										connect: {
											url: media.url,
										},
									},
								},
							});
							break;
						}
						case 'eventPoster': {
							await ctx.prisma.event.update({
								where: {
									id: data.entityId,
								},
								data: {
									poster: {
										connect: {
											url: media.url,
										},
									},
								},
							});
							break;
						}
						default: {
							break;
						}
					}
				}

				return {
					fileName: result.fileName,
					signedURL: result.signedURL,
					mediaId: media.id,
					mediaURL: media.url,
				};
			},
		});
	},
});

export const createMediaInput = inputObjectType({
	name: 'createMediaInput',
	definition(t) {
		t.nonNull.string('fileType');
		t.int('entityId');
		t.field('entityType', {
			type: 'entityTypes',
		});
	},
});

export const getMediaDataInput = inputObjectType({
	name: 'getMediaDataInput',
	definition(t) {
		t.int('entityId');
		t.string('fileName');
		t.string('fileType');
	},
});