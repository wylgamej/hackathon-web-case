import { arg, extendType, inputObjectType, nonNull } from 'nexus';
import { Context } from '../../graphql/context';

export const MediaQuery = extendType({
	type: 'Query',
	definition: (t) => {
		t.crud.media({
			filtering: true,
		});
	},
});
