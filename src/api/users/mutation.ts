import { Context } from '../../graphql/context';
import { extendType, arg, inputObjectType, nonNull } from 'nexus';
import { createRefreshToken } from '../../integrations/jwt';

export const ClientMutation = extendType({
	type: 'Mutation',
	definition: (t) => {
		t.field('signUp', {
			type: 'AuthPayload',
			args: { data: nonNull(arg({ type: SignUpInput })) },
			resolve: async (_, { data }, { dataSources, response }) => {
				const payload = await dataSources.usersAPI.createUser(data);

				response.cookie('jid', createRefreshToken(payload.user), { httpOnly: true, secure: process.env.ENV_NAME === 'PRODUCTION' });

				return payload;
			},
		});
		t.field('signIn', {
			type: 'AuthPayload',
			args: { data: nonNull(arg({ type: SignInInput })) },
			resolve: async (_, { data }, { dataSources, response }) => {
				const payload = await dataSources.usersAPI.signIn(data);

				response.cookie('jid', createRefreshToken(payload.user), { httpOnly: true, secure: process.env.ENV_NAME === 'PRODUCTION' });

				return payload;
			},
		});
		t.field('updateUser', {
			type: 'String',
			args: { data: nonNull(arg({ type: updateUserInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				const userData = await ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
				});

				await ctx.prisma.user.update({
					where: {
						id: ctx.user.id,
					},
					data: {
						firstname: data.firstname !== null ? data.firstname : userData.firstname,
						lastname: data.lastname !== null ? data.lastname : userData.lastname,
						email: data.email !== null ? data.email : userData.email,
						telegrammLink: data.telegrammLink !== null ? data.telegrammLink : userData.telegrammLink,
						work: data.work !== null ? data.work : userData.work,
						phone: data.phone !== null ? data.phone : userData.phone,
					},
				});

				return 'Success!';
			}
		})
	},
});

export const SignUpInput = inputObjectType({
	name: 'CreateUserInput',
	definition(t) {
		t.nonNull.string('firstname');
		t.nonNull.string('lastname');
		t.nonNull.string('login');
		t.nonNull.string('email');
		t.nonNull.string('password');
	},
});

export const SignInInput = inputObjectType({
	name: 'SignInInput',
	definition(t) {
		t.nonNull.string('login');
		t.nonNull.string('password');
	},
});

export const updateUserInput = inputObjectType({
	name: 'updateUserInput',
	definition(t) {
		t.string('email');
		t.string('firstname');
		t.string('lastname');
		t.string('telegrammLink');
		t.string('work');
		t.string('phone');
	}
})

