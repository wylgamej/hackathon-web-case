import { arg, extendType, inputObjectType, nonNull } from 'nexus';
import { Context } from '../../graphql/context';

export const UserQuery = extendType({
	type: 'Query',
	definition: (t) => {
		t.field('getUser', {
			type: 'User',
			args: { data: nonNull(arg({ type: getUserInput })) },
			resolve: async (_, { data }, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: data.userId,
					},
				});
			},
		});
		t.field('getMyEvents', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						myevent: true,
					},
				});
			},
		});
		t.field('getMyPlanning', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						planning: true,
					},
				});
			},
		});
		t.field('getMyParticipians', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						participian: true,
					},
				});
			},
		});
		t.field('getMyArchives', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						archives: true,
					},
				});
			},
		});
		t.field('getMyGuests', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						guest: true,
					},
				});
			},
		});
		t.field('getMyMaybeGo', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						maybego: true,
					},
				});
			},
		});
		t.field('me', {
			type: 'User',
			resolve: async (_, __, ctx: Context) => {
				const events = await ctx.prisma.event.findMany({
					where: {
						members: {
							some: {
								id: ctx.user.id,
							},
						},
					},
				});

				events.forEach(async element => {
					if (element.date < new Date()) {
						await ctx.prisma.event.update({
							where: {
								id: element.id,
							},
							data: {
								members: {
									disconnect: {
										id: ctx.user.id,
									},
								},
								visitors: {
									connect: {
										id: ctx.user.id,
									},
								},
							}
						});
					};
				});

				return ctx.prisma.user.findUnique({
					where: {
						id: ctx.user.id,
					},
					include: {
						planning: true,
						myevent: true,
						guest: true,
						participian: true,
						maybego: true,
					},
				});
			},
		});
	},
});

export const getUserInput = inputObjectType({
	name: 'getUserInput',
	definition(t) {
		t.nonNull.int('userId');
	},
});