import { enumType, objectType } from 'nexus';

export * from './query';
export * from './mutation';

export const User = objectType({
	name: 'User',
	definition(t) {
		t.model.id();
		t.model.email();
		t.model.login();
		t.model.password();
		t.model.firstname();
		t.model.lastname();
		t.model.telegrammLink();
		t.model.work();
		t.model.phone();
		t.model.tokenVersion();
		t.model.role();
		t.model.avatar();
		t.model.myevent();
		t.model.planning();
		t.model.participian();
		t.model.archives();
		t.model.guest();
		t.model.maybego();
	},
});

export const AuthPayload = objectType({
	name: 'AuthPayload',
	definition(t) {
		t.string('token');
		t.field('user', { type: 'User' });
	},
});

export const registerType = enumType({
	name: 'registerType',
	members: ['participians', 'guests'],
});