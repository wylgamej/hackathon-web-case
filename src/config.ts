/* eslint-disable no-magic-numbers */
export const PORT = process.env.PORT;
export const JWT_SECRET = process.env.JWT_SECRET;
export const JWT_REFRESH_SECRET = process.env.JWT_REFRESH_SECRET;
export const AWS_REGION = process.env.AWS_REGION;
export const AWS_BUCKET_NAME = process.env.AWS_BUCKET_NAME;
export const AWS_USER_KEY = process.env.AWS_USER_KEY;
export const AWS_USER_SECRET_KEY = process.env.AWS_USER_SECRET_KEY;
export const SIGN_URL_EXPIRES = process.env.SIGN_URL_EXPIRES;
export const DEFAULT_IMAGE = process.env.DEFAULT_IMAGE;
export const BASE_URL = process.env.BASE_URL;
export const MAIL_URL = process.env.MAIL_URL;
export const MAIL_PASS = process.env.MAIL_PASS;