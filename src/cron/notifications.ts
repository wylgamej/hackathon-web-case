import * as cron from 'node-cron';
import { sendEMail } from '../integrations/email';
import prisma from '../prisma-client';

export const allCronFunctions = () => {
	cron.schedule('0 30 12 * * *', async () => {
		sendNotifications();
	});

	cron.schedule('0 0 4 * * *', async () => {
		eventDistribution();
	})
}

export const eventDistribution = async () => {
	const users = await prisma.user.findMany({
		include: {
			myevent: true,
			planning: true,
		},
	});

	await users.forEach(async element => {
		element.planning.forEach(async event => {
			if (event.date.getTime() - new Date().getTime() <= 0) {
				await prisma.user.update({
					where: {
						id: element.id,
					},
					data: {
						planning: {
							disconnect: {
								id: event.id,
							},
						},
						archives: {
							connect: {
								id: event.id,
							},
						},
					},
				});
			}
		})
		element.myevent.forEach(async event => {
			if (event.date.getTime() - new Date().getTime() > 0) {
				await prisma.user.update({
					where: {
						id: element.id,
					},
					data: {
						planning: {
							connect: {
								id: event.id,
							},
						},
					},
				});
			}
			else if (event.date.getTime() - new Date().getTime() <= 0) {
				await prisma.user.update({
					where: {
						id: element.id,
					},
					data: {
						planning: {
							disconnect: {
								id: event.id,
							},
						},
						archives: {
							connect: {
								id: event.id,
							},
						},
					},
				});
			}
		});
	})
}

export const sendNotifications = async () => {
	const users = await prisma.user.findMany({
		include: {
			planning: true,
		},
	});
	await users.forEach(async element => {
		await element.planning.forEach(async event => {
			if (event.date.getTime() - new Date().getTime() <= 172800000 && event.date.getTime() - new Date().getTime() > 0) {
				const mailOptions: SendingMail = {
					from: process.env.MAIL_URL || 'wylgamej@gmail.com',
					to: element.email,
					subject: 'Скоро запланированное мероприятие! ' + event.title,
					text: event.description
				}
				await sendEMail(mailOptions);
				console.log('Send Notification:' + element.email + 'About:' + event.title);
			}
		});
	});
}

interface SendingMail {
	from: String,
	to: String,
	subject: String,
	text: String,
};