/**
 * This file was generated by Nexus Schema
 * Do not make changes to this file directly
 */





declare global {
  interface NexusGenCustomOutputProperties<TypeName extends string> {
    crud: NexusPrisma<TypeName, 'crud'>
    model: NexusPrisma<TypeName, 'model'>
  }
}

declare global {
  interface NexusGen extends NexusGenTypes {}
}

export interface NexusGenInputs {
  BoolFilter: { // input type
    equals?: boolean | null; // Boolean
    not?: NexusGenInputs['NestedBoolFilter'] | null; // NestedBoolFilter
  }
  CreateUserInput: { // input type
    email: string; // String!
    firstname: string; // String!
    lastname: string; // String!
    login: string; // String!
    password: string; // String!
  }
  DateTimeFilter: { // input type
    equals?: NexusGenScalars['DateTime'] | null; // DateTime
    gt?: NexusGenScalars['DateTime'] | null; // DateTime
    gte?: NexusGenScalars['DateTime'] | null; // DateTime
    in?: NexusGenScalars['DateTime'][] | null; // [DateTime!]
    lt?: NexusGenScalars['DateTime'] | null; // DateTime
    lte?: NexusGenScalars['DateTime'] | null; // DateTime
    not?: NexusGenInputs['NestedDateTimeFilter'] | null; // NestedDateTimeFilter
    notIn?: NexusGenScalars['DateTime'][] | null; // [DateTime!]
  }
  EnumeventFormatNullableFilter: { // input type
    equals?: NexusGenEnums['eventFormat'] | null; // eventFormat
    in?: NexusGenEnums['eventFormat'][] | null; // [eventFormat!]
    not?: NexusGenInputs['NestedEnumeventFormatNullableFilter'] | null; // NestedEnumeventFormatNullableFilter
    notIn?: NexusGenEnums['eventFormat'][] | null; // [eventFormat!]
  }
  EnumeventTypeNullableFilter: { // input type
    equals?: NexusGenEnums['eventType'] | null; // eventType
    in?: NexusGenEnums['eventType'][] | null; // [eventType!]
    not?: NexusGenInputs['NestedEnumeventTypeNullableFilter'] | null; // NestedEnumeventTypeNullableFilter
    notIn?: NexusGenEnums['eventType'][] | null; // [eventType!]
  }
  EnumuserRoleFilter: { // input type
    equals?: NexusGenEnums['userRole'] | null; // userRole
    in?: NexusGenEnums['userRole'][] | null; // [userRole!]
    not?: NexusGenInputs['NestedEnumuserRoleFilter'] | null; // NestedEnumuserRoleFilter
    notIn?: NexusGenEnums['userRole'][] | null; // [userRole!]
  }
  EventListRelationFilter: { // input type
    every?: NexusGenInputs['EventWhereInput'] | null; // EventWhereInput
    none?: NexusGenInputs['EventWhereInput'] | null; // EventWhereInput
    some?: NexusGenInputs['EventWhereInput'] | null; // EventWhereInput
  }
  EventOrderByRelationAggregateInput: { // input type
    _count?: NexusGenEnums['SortOrder'] | null; // SortOrder
  }
  EventOrderByWithRelationInput: { // input type
    author?: NexusGenInputs['UserOrderByWithRelationInput'] | null; // UserOrderByWithRelationInput
    authorId?: NexusGenEnums['SortOrder'] | null; // SortOrder
    date?: NexusGenEnums['SortOrder'] | null; // SortOrder
    description?: NexusGenEnums['SortOrder'] | null; // SortOrder
    format?: NexusGenEnums['SortOrder'] | null; // SortOrder
    guests?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
    helpers?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
    id?: NexusGenEnums['SortOrder'] | null; // SortOrder
    inviteLink?: NexusGenEnums['SortOrder'] | null; // SortOrder
    maybe?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
    members?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
    organizer?: NexusGenEnums['SortOrder'] | null; // SortOrder
    participians?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
    place?: NexusGenEnums['SortOrder'] | null; // SortOrder
    poster?: NexusGenInputs['MediaOrderByWithRelationInput'] | null; // MediaOrderByWithRelationInput
    posterId?: NexusGenEnums['SortOrder'] | null; // SortOrder
    seats?: NexusGenEnums['SortOrder'] | null; // SortOrder
    shortDescription?: NexusGenEnums['SortOrder'] | null; // SortOrder
    telegramm?: NexusGenEnums['SortOrder'] | null; // SortOrder
    title?: NexusGenEnums['SortOrder'] | null; // SortOrder
    type?: NexusGenEnums['SortOrder'] | null; // SortOrder
    visitors?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
  }
  EventWhereInput: { // input type
    AND?: NexusGenInputs['EventWhereInput'][] | null; // [EventWhereInput!]
    NOT?: NexusGenInputs['EventWhereInput'][] | null; // [EventWhereInput!]
    OR?: NexusGenInputs['EventWhereInput'][] | null; // [EventWhereInput!]
    author?: NexusGenInputs['UserWhereInput'] | null; // UserWhereInput
    authorId?: NexusGenInputs['IntFilter'] | null; // IntFilter
    date?: NexusGenInputs['DateTimeFilter'] | null; // DateTimeFilter
    description?: NexusGenInputs['StringFilter'] | null; // StringFilter
    format?: NexusGenInputs['EnumeventFormatNullableFilter'] | null; // EnumeventFormatNullableFilter
    guests?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
    helpers?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
    id?: NexusGenInputs['IntFilter'] | null; // IntFilter
    inviteLink?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    maybe?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
    members?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
    organizer?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    participians?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
    place?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    poster?: NexusGenInputs['MediaWhereInput'] | null; // MediaWhereInput
    posterId?: NexusGenInputs['IntNullableFilter'] | null; // IntNullableFilter
    seats?: NexusGenInputs['IntNullableFilter'] | null; // IntNullableFilter
    shortDescription?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    telegramm?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    title?: NexusGenInputs['StringFilter'] | null; // StringFilter
    type?: NexusGenInputs['EnumeventTypeNullableFilter'] | null; // EnumeventTypeNullableFilter
    visitors?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
  }
  EventWhereUniqueInput: { // input type
    id?: number | null; // Int
    inviteLink?: string | null; // String
  }
  IntFilter: { // input type
    equals?: number | null; // Int
    gt?: number | null; // Int
    gte?: number | null; // Int
    in?: number[] | null; // [Int!]
    lt?: number | null; // Int
    lte?: number | null; // Int
    not?: NexusGenInputs['NestedIntFilter'] | null; // NestedIntFilter
    notIn?: number[] | null; // [Int!]
  }
  IntNullableFilter: { // input type
    equals?: number | null; // Int
    gt?: number | null; // Int
    gte?: number | null; // Int
    in?: number[] | null; // [Int!]
    lt?: number | null; // Int
    lte?: number | null; // Int
    not?: NexusGenInputs['NestedIntNullableFilter'] | null; // NestedIntNullableFilter
    notIn?: number[] | null; // [Int!]
  }
  MediaOrderByWithRelationInput: { // input type
    createdAt?: NexusGenEnums['SortOrder'] | null; // SortOrder
    events?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    id?: NexusGenEnums['SortOrder'] | null; // SortOrder
    isApproved?: NexusGenEnums['SortOrder'] | null; // SortOrder
    url?: NexusGenEnums['SortOrder'] | null; // SortOrder
    users?: NexusGenInputs['UserOrderByRelationAggregateInput'] | null; // UserOrderByRelationAggregateInput
  }
  MediaWhereInput: { // input type
    AND?: NexusGenInputs['MediaWhereInput'][] | null; // [MediaWhereInput!]
    NOT?: NexusGenInputs['MediaWhereInput'][] | null; // [MediaWhereInput!]
    OR?: NexusGenInputs['MediaWhereInput'][] | null; // [MediaWhereInput!]
    createdAt?: NexusGenInputs['DateTimeFilter'] | null; // DateTimeFilter
    events?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    id?: NexusGenInputs['IntFilter'] | null; // IntFilter
    isApproved?: NexusGenInputs['BoolFilter'] | null; // BoolFilter
    url?: NexusGenInputs['StringFilter'] | null; // StringFilter
    users?: NexusGenInputs['UserListRelationFilter'] | null; // UserListRelationFilter
  }
  MediaWhereUniqueInput: { // input type
    id?: number | null; // Int
    url?: string | null; // String
  }
  NestedBoolFilter: { // input type
    equals?: boolean | null; // Boolean
    not?: NexusGenInputs['NestedBoolFilter'] | null; // NestedBoolFilter
  }
  NestedDateTimeFilter: { // input type
    equals?: NexusGenScalars['DateTime'] | null; // DateTime
    gt?: NexusGenScalars['DateTime'] | null; // DateTime
    gte?: NexusGenScalars['DateTime'] | null; // DateTime
    in?: NexusGenScalars['DateTime'][] | null; // [DateTime!]
    lt?: NexusGenScalars['DateTime'] | null; // DateTime
    lte?: NexusGenScalars['DateTime'] | null; // DateTime
    not?: NexusGenInputs['NestedDateTimeFilter'] | null; // NestedDateTimeFilter
    notIn?: NexusGenScalars['DateTime'][] | null; // [DateTime!]
  }
  NestedEnumeventFormatNullableFilter: { // input type
    equals?: NexusGenEnums['eventFormat'] | null; // eventFormat
    in?: NexusGenEnums['eventFormat'][] | null; // [eventFormat!]
    not?: NexusGenInputs['NestedEnumeventFormatNullableFilter'] | null; // NestedEnumeventFormatNullableFilter
    notIn?: NexusGenEnums['eventFormat'][] | null; // [eventFormat!]
  }
  NestedEnumeventTypeNullableFilter: { // input type
    equals?: NexusGenEnums['eventType'] | null; // eventType
    in?: NexusGenEnums['eventType'][] | null; // [eventType!]
    not?: NexusGenInputs['NestedEnumeventTypeNullableFilter'] | null; // NestedEnumeventTypeNullableFilter
    notIn?: NexusGenEnums['eventType'][] | null; // [eventType!]
  }
  NestedEnumuserRoleFilter: { // input type
    equals?: NexusGenEnums['userRole'] | null; // userRole
    in?: NexusGenEnums['userRole'][] | null; // [userRole!]
    not?: NexusGenInputs['NestedEnumuserRoleFilter'] | null; // NestedEnumuserRoleFilter
    notIn?: NexusGenEnums['userRole'][] | null; // [userRole!]
  }
  NestedIntFilter: { // input type
    equals?: number | null; // Int
    gt?: number | null; // Int
    gte?: number | null; // Int
    in?: number[] | null; // [Int!]
    lt?: number | null; // Int
    lte?: number | null; // Int
    not?: NexusGenInputs['NestedIntFilter'] | null; // NestedIntFilter
    notIn?: number[] | null; // [Int!]
  }
  NestedIntNullableFilter: { // input type
    equals?: number | null; // Int
    gt?: number | null; // Int
    gte?: number | null; // Int
    in?: number[] | null; // [Int!]
    lt?: number | null; // Int
    lte?: number | null; // Int
    not?: NexusGenInputs['NestedIntNullableFilter'] | null; // NestedIntNullableFilter
    notIn?: number[] | null; // [Int!]
  }
  NestedStringFilter: { // input type
    contains?: string | null; // String
    endsWith?: string | null; // String
    equals?: string | null; // String
    gt?: string | null; // String
    gte?: string | null; // String
    in?: string[] | null; // [String!]
    lt?: string | null; // String
    lte?: string | null; // String
    not?: NexusGenInputs['NestedStringFilter'] | null; // NestedStringFilter
    notIn?: string[] | null; // [String!]
    startsWith?: string | null; // String
  }
  NestedStringNullableFilter: { // input type
    contains?: string | null; // String
    endsWith?: string | null; // String
    equals?: string | null; // String
    gt?: string | null; // String
    gte?: string | null; // String
    in?: string[] | null; // [String!]
    lt?: string | null; // String
    lte?: string | null; // String
    not?: NexusGenInputs['NestedStringNullableFilter'] | null; // NestedStringNullableFilter
    notIn?: string[] | null; // [String!]
    startsWith?: string | null; // String
  }
  SignInInput: { // input type
    login: string; // String!
    password: string; // String!
  }
  StringFilter: { // input type
    contains?: string | null; // String
    endsWith?: string | null; // String
    equals?: string | null; // String
    gt?: string | null; // String
    gte?: string | null; // String
    in?: string[] | null; // [String!]
    lt?: string | null; // String
    lte?: string | null; // String
    mode?: NexusGenEnums['QueryMode'] | null; // QueryMode
    not?: NexusGenInputs['NestedStringFilter'] | null; // NestedStringFilter
    notIn?: string[] | null; // [String!]
    startsWith?: string | null; // String
  }
  StringNullableFilter: { // input type
    contains?: string | null; // String
    endsWith?: string | null; // String
    equals?: string | null; // String
    gt?: string | null; // String
    gte?: string | null; // String
    in?: string[] | null; // [String!]
    lt?: string | null; // String
    lte?: string | null; // String
    mode?: NexusGenEnums['QueryMode'] | null; // QueryMode
    not?: NexusGenInputs['NestedStringNullableFilter'] | null; // NestedStringNullableFilter
    notIn?: string[] | null; // [String!]
    startsWith?: string | null; // String
  }
  UserListRelationFilter: { // input type
    every?: NexusGenInputs['UserWhereInput'] | null; // UserWhereInput
    none?: NexusGenInputs['UserWhereInput'] | null; // UserWhereInput
    some?: NexusGenInputs['UserWhereInput'] | null; // UserWhereInput
  }
  UserOrderByRelationAggregateInput: { // input type
    _count?: NexusGenEnums['SortOrder'] | null; // SortOrder
  }
  UserOrderByWithRelationInput: { // input type
    archives?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    avatar?: NexusGenInputs['MediaOrderByWithRelationInput'] | null; // MediaOrderByWithRelationInput
    avatarId?: NexusGenEnums['SortOrder'] | null; // SortOrder
    email?: NexusGenEnums['SortOrder'] | null; // SortOrder
    firstname?: NexusGenEnums['SortOrder'] | null; // SortOrder
    guest?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    helper?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    id?: NexusGenEnums['SortOrder'] | null; // SortOrder
    lastname?: NexusGenEnums['SortOrder'] | null; // SortOrder
    login?: NexusGenEnums['SortOrder'] | null; // SortOrder
    maybego?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    myevent?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    participian?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    password?: NexusGenEnums['SortOrder'] | null; // SortOrder
    phone?: NexusGenEnums['SortOrder'] | null; // SortOrder
    planning?: NexusGenInputs['EventOrderByRelationAggregateInput'] | null; // EventOrderByRelationAggregateInput
    role?: NexusGenEnums['SortOrder'] | null; // SortOrder
    telegrammLink?: NexusGenEnums['SortOrder'] | null; // SortOrder
    tokenVersion?: NexusGenEnums['SortOrder'] | null; // SortOrder
    work?: NexusGenEnums['SortOrder'] | null; // SortOrder
  }
  UserWhereInput: { // input type
    AND?: NexusGenInputs['UserWhereInput'][] | null; // [UserWhereInput!]
    NOT?: NexusGenInputs['UserWhereInput'][] | null; // [UserWhereInput!]
    OR?: NexusGenInputs['UserWhereInput'][] | null; // [UserWhereInput!]
    archives?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    avatar?: NexusGenInputs['MediaWhereInput'] | null; // MediaWhereInput
    avatarId?: NexusGenInputs['IntNullableFilter'] | null; // IntNullableFilter
    email?: NexusGenInputs['StringFilter'] | null; // StringFilter
    firstname?: NexusGenInputs['StringFilter'] | null; // StringFilter
    guest?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    helper?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    id?: NexusGenInputs['IntFilter'] | null; // IntFilter
    lastname?: NexusGenInputs['StringFilter'] | null; // StringFilter
    login?: NexusGenInputs['StringFilter'] | null; // StringFilter
    maybego?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    myevent?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    participian?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    password?: NexusGenInputs['StringFilter'] | null; // StringFilter
    phone?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    planning?: NexusGenInputs['EventListRelationFilter'] | null; // EventListRelationFilter
    role?: NexusGenInputs['EnumuserRoleFilter'] | null; // EnumuserRoleFilter
    telegrammLink?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
    tokenVersion?: NexusGenInputs['IntNullableFilter'] | null; // IntNullableFilter
    work?: NexusGenInputs['StringNullableFilter'] | null; // StringNullableFilter
  }
  UserWhereUniqueInput: { // input type
    email?: string | null; // String
    id?: number | null; // Int
    login?: string | null; // String
  }
  createInviteInput: { // input type
    eventId: number; // Int!
  }
  createMediaInput: { // input type
    entityId?: number | null; // Int
    entityType?: NexusGenEnums['entityTypes'] | null; // entityTypes
    fileType: string; // String!
  }
  createOneEventInput: { // input type
    date: NexusGenScalars['DateTime']; // DateTime!
    description: string; // String!
    format?: NexusGenEnums['eventFormat'] | null; // eventFormat
    organizer: string; // String!
    place?: string | null; // String
    repeat?: boolean | null; // Boolean
    seats?: number | null; // Int
    shortDescription: string; // String!
    telegramm?: string | null; // String
    title: string; // String!
    type?: NexusGenEnums['eventType'] | null; // eventType
  }
  deleteOneEventInput: { // input type
    eventId: number; // Int!
  }
  getEventInput: { // input type
    eventId: number; // Int!
  }
  getEventStatisticInput: { // input type
    eventId: number; // Int!
  }
  getMediaDataInput: { // input type
    entityId?: number | null; // Int
    fileName?: string | null; // String
    fileType?: string | null; // String
  }
  getUserInput: { // input type
    userId: number; // Int!
  }
  registerForEventInput: { // input type
    decided: NexusGenEnums['registerDecided']; // registerDecided!
    eventId: number; // Int!
    registerType?: NexusGenEnums['registerType'] | null; // registerType
  }
  updateOneEventInput: { // input type
    date?: NexusGenScalars['DateTime'] | null; // DateTime
    description?: string | null; // String
    eventId: number; // Int!
    format?: NexusGenEnums['eventFormat'] | null; // eventFormat
    organizer?: string | null; // String
    place?: string | null; // String
    seats?: number | null; // Int
    shortDescription?: string | null; // String
    telegramm?: string | null; // String
    title?: string | null; // String
    type?: NexusGenEnums['eventType'] | null; // eventType
  }
  updateUserInput: { // input type
    email?: string | null; // String
    firstname?: string | null; // String
    lastname?: string | null; // String
    phone?: string | null; // String
    telegrammLink?: string | null; // String
    work?: string | null; // String
  }
}

export interface NexusGenEnums {
  QueryMode: "default" | "insensitive"
  SortOrder: "asc" | "desc"
  entityTypes: "eventPoster" | "userAvatar"
  eventFormat: "offline" | "online"
  eventType: "command" | "general" | "local"
  registerDecided: "maybego" | "willgo"
  registerType: "guests" | "participians"
  userRole: "common" | "moderator"
}

export interface NexusGenScalars {
  String: string
  Int: number
  Float: number
  Boolean: boolean
  ID: string
  DateTime: any
}

export interface NexusGenObjects {
  AuthPayload: { // root type
    token?: string | null; // String
    user?: NexusGenRootTypes['User'] | null; // User
  }
  Event: { // root type
    date: NexusGenScalars['DateTime']; // DateTime!
    description: string; // String!
    format?: NexusGenEnums['eventFormat'] | null; // eventFormat
    id: number; // Int!
    inviteLink?: string | null; // String
    organizer?: string | null; // String
    place?: string | null; // String
    seats?: number | null; // Int
    shortDescription?: string | null; // String
    statistic?: NexusGenRootTypes['EventStatistic'] | null; // EventStatistic
    telegramm?: string | null; // String
    title: string; // String!
    type?: NexusGenEnums['eventType'] | null; // eventType
  }
  EventStatistic: { // root type
    allMembers?: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    allMembersCount?: number | null; // Int
    allMembersPercentToAllMembersLastEvent?: number | null; // Int
    allMembersPercentToAllPeople?: number | null; // Int
    date?: NexusGenScalars['DateTime'] | null; // DateTime
    helpers?: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    helpersCount?: number | null; // Int
    maybeGo?: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    maybeGoCount?: number | null; // Int
    maybeGoPercentToAllMembers?: number | null; // Int
    place?: string | null; // String
    willGo?: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    willGoCount?: number | null; // Int
    willGoPercentToAllMembers?: number | null; // Int
  }
  Media: { // root type
    createdAt: NexusGenScalars['DateTime']; // DateTime!
    id: number; // Int!
  }
  Mutation: {};
  Query: {};
  SignUrlResponse: { // root type
    mediaId?: number | null; // Int
    mediaURL?: string | null; // String
    signedURL?: string | null; // String
  }
  User: { // root type
    email: string; // String!
    firstname: string; // String!
    id: number; // Int!
    lastname: string; // String!
    login: string; // String!
    password: string; // String!
    phone?: string | null; // String
    role: NexusGenEnums['userRole']; // userRole!
    telegrammLink?: string | null; // String
    tokenVersion?: number | null; // Int
    work?: string | null; // String
  }
}

export interface NexusGenInterfaces {
}

export interface NexusGenUnions {
}

export type NexusGenRootTypes = NexusGenObjects

export type NexusGenAllTypes = NexusGenRootTypes & NexusGenScalars & NexusGenEnums

export interface NexusGenFieldTypes {
  AuthPayload: { // field return type
    token: string | null; // String
    user: NexusGenRootTypes['User'] | null; // User
  }
  Event: { // field return type
    author: NexusGenRootTypes['User']; // User!
    date: NexusGenScalars['DateTime']; // DateTime!
    description: string; // String!
    format: NexusGenEnums['eventFormat'] | null; // eventFormat
    guests: NexusGenRootTypes['User'][]; // [User!]!
    id: number; // Int!
    inviteLink: string | null; // String
    maybe: NexusGenRootTypes['User'][]; // [User!]!
    members: NexusGenRootTypes['User'][]; // [User!]!
    organizer: string | null; // String
    participians: NexusGenRootTypes['User'][]; // [User!]!
    place: string | null; // String
    poster: NexusGenRootTypes['Media'] | null; // Media
    seats: number | null; // Int
    shortDescription: string | null; // String
    statistic: NexusGenRootTypes['EventStatistic'] | null; // EventStatistic
    telegramm: string | null; // String
    title: string; // String!
    type: NexusGenEnums['eventType'] | null; // eventType
    visitors: NexusGenRootTypes['User'][]; // [User!]!
  }
  EventStatistic: { // field return type
    allMembers: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    allMembersCount: number | null; // Int
    allMembersPercentToAllMembersLastEvent: number | null; // Int
    allMembersPercentToAllPeople: number | null; // Int
    date: NexusGenScalars['DateTime'] | null; // DateTime
    helpers: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    helpersCount: number | null; // Int
    maybeGo: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    maybeGoCount: number | null; // Int
    maybeGoPercentToAllMembers: number | null; // Int
    place: string | null; // String
    willGo: Array<NexusGenRootTypes['User'] | null> | null; // [User]
    willGoCount: number | null; // Int
    willGoPercentToAllMembers: number | null; // Int
  }
  Media: { // field return type
    createdAt: NexusGenScalars['DateTime']; // DateTime!
    events: NexusGenRootTypes['Event'][]; // [Event!]!
    id: number; // Int!
    link: string | null; // String
    users: NexusGenRootTypes['User'][]; // [User!]!
  }
  Mutation: { // field return type
    createInviteToEvent: string | null; // String
    createMedia: NexusGenRootTypes['SignUrlResponse'] | null; // SignUrlResponse
    createOneEvent: string | null; // String
    deleteOneEvent: string | null; // String
    putEventPoster: NexusGenRootTypes['SignUrlResponse'] | null; // SignUrlResponse
    putUserAvatar: NexusGenRootTypes['SignUrlResponse'] | null; // SignUrlResponse
    registerForEvent: string | null; // String
    signIn: NexusGenRootTypes['AuthPayload'] | null; // AuthPayload
    signUp: NexusGenRootTypes['AuthPayload'] | null; // AuthPayload
    updateOneEvent: string | null; // String
    updateUser: string | null; // String
  }
  Query: { // field return type
    events: NexusGenRootTypes['Event'][]; // [Event!]!
    getAllEvents: Array<NexusGenRootTypes['Event'] | null> | null; // [Event]
    getEvent: NexusGenRootTypes['Event'] | null; // Event
    getEventStatistic: NexusGenRootTypes['EventStatistic'] | null; // EventStatistic
    getMyArchives: NexusGenRootTypes['User'] | null; // User
    getMyEvents: NexusGenRootTypes['User'] | null; // User
    getMyGuests: NexusGenRootTypes['User'] | null; // User
    getMyMaybeGo: NexusGenRootTypes['User'] | null; // User
    getMyParticipians: NexusGenRootTypes['User'] | null; // User
    getMyPlanning: NexusGenRootTypes['User'] | null; // User
    getUser: NexusGenRootTypes['User'] | null; // User
    me: NexusGenRootTypes['User'] | null; // User
    media: NexusGenRootTypes['Media'][]; // [Media!]!
  }
  SignUrlResponse: { // field return type
    mediaId: number | null; // Int
    mediaURL: string | null; // String
    signedURL: string | null; // String
  }
  User: { // field return type
    archives: NexusGenRootTypes['Event'][]; // [Event!]!
    avatar: NexusGenRootTypes['Media'] | null; // Media
    email: string; // String!
    firstname: string; // String!
    guest: NexusGenRootTypes['Event'][]; // [Event!]!
    id: number; // Int!
    lastname: string; // String!
    login: string; // String!
    maybego: NexusGenRootTypes['Event'][]; // [Event!]!
    myevent: NexusGenRootTypes['Event'][]; // [Event!]!
    participian: NexusGenRootTypes['Event'][]; // [Event!]!
    password: string; // String!
    phone: string | null; // String
    planning: NexusGenRootTypes['Event'][]; // [Event!]!
    role: NexusGenEnums['userRole']; // userRole!
    telegrammLink: string | null; // String
    tokenVersion: number | null; // Int
    work: string | null; // String
  }
}

export interface NexusGenFieldTypeNames {
  AuthPayload: { // field return type name
    token: 'String'
    user: 'User'
  }
  Event: { // field return type name
    author: 'User'
    date: 'DateTime'
    description: 'String'
    format: 'eventFormat'
    guests: 'User'
    id: 'Int'
    inviteLink: 'String'
    maybe: 'User'
    members: 'User'
    organizer: 'String'
    participians: 'User'
    place: 'String'
    poster: 'Media'
    seats: 'Int'
    shortDescription: 'String'
    statistic: 'EventStatistic'
    telegramm: 'String'
    title: 'String'
    type: 'eventType'
    visitors: 'User'
  }
  EventStatistic: { // field return type name
    allMembers: 'User'
    allMembersCount: 'Int'
    allMembersPercentToAllMembersLastEvent: 'Int'
    allMembersPercentToAllPeople: 'Int'
    date: 'DateTime'
    helpers: 'User'
    helpersCount: 'Int'
    maybeGo: 'User'
    maybeGoCount: 'Int'
    maybeGoPercentToAllMembers: 'Int'
    place: 'String'
    willGo: 'User'
    willGoCount: 'Int'
    willGoPercentToAllMembers: 'Int'
  }
  Media: { // field return type name
    createdAt: 'DateTime'
    events: 'Event'
    id: 'Int'
    link: 'String'
    users: 'User'
  }
  Mutation: { // field return type name
    createInviteToEvent: 'String'
    createMedia: 'SignUrlResponse'
    createOneEvent: 'String'
    deleteOneEvent: 'String'
    putEventPoster: 'SignUrlResponse'
    putUserAvatar: 'SignUrlResponse'
    registerForEvent: 'String'
    signIn: 'AuthPayload'
    signUp: 'AuthPayload'
    updateOneEvent: 'String'
    updateUser: 'String'
  }
  Query: { // field return type name
    events: 'Event'
    getAllEvents: 'Event'
    getEvent: 'Event'
    getEventStatistic: 'EventStatistic'
    getMyArchives: 'User'
    getMyEvents: 'User'
    getMyGuests: 'User'
    getMyMaybeGo: 'User'
    getMyParticipians: 'User'
    getMyPlanning: 'User'
    getUser: 'User'
    me: 'User'
    media: 'Media'
  }
  SignUrlResponse: { // field return type name
    mediaId: 'Int'
    mediaURL: 'String'
    signedURL: 'String'
  }
  User: { // field return type name
    archives: 'Event'
    avatar: 'Media'
    email: 'String'
    firstname: 'String'
    guest: 'Event'
    id: 'Int'
    lastname: 'String'
    login: 'String'
    maybego: 'Event'
    myevent: 'Event'
    participian: 'Event'
    password: 'String'
    phone: 'String'
    planning: 'Event'
    role: 'userRole'
    telegrammLink: 'String'
    tokenVersion: 'Int'
    work: 'String'
  }
}

export interface NexusGenArgTypes {
  Event: {
    guests: { // args
      after?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      before?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    maybe: { // args
      after?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      before?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    members: { // args
      after?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      before?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    participians: { // args
      after?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      before?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    visitors: { // args
      after?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      before?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
  }
  Media: {
    events: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    users: { // args
      after?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      before?: NexusGenInputs['UserWhereUniqueInput'] | null; // UserWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
  }
  Mutation: {
    createInviteToEvent: { // args
      data: NexusGenInputs['createInviteInput']; // createInviteInput!
    }
    createMedia: { // args
      data: NexusGenInputs['createMediaInput']; // createMediaInput!
    }
    createOneEvent: { // args
      data: NexusGenInputs['createOneEventInput']; // createOneEventInput!
    }
    deleteOneEvent: { // args
      data: NexusGenInputs['deleteOneEventInput']; // deleteOneEventInput!
    }
    putEventPoster: { // args
      data: NexusGenInputs['getMediaDataInput']; // getMediaDataInput!
    }
    putUserAvatar: { // args
      data: NexusGenInputs['getMediaDataInput']; // getMediaDataInput!
    }
    registerForEvent: { // args
      data: NexusGenInputs['registerForEventInput']; // registerForEventInput!
    }
    signIn: { // args
      data: NexusGenInputs['SignInInput']; // SignInInput!
    }
    signUp: { // args
      data: NexusGenInputs['CreateUserInput']; // CreateUserInput!
    }
    updateOneEvent: { // args
      data: NexusGenInputs['updateOneEventInput']; // updateOneEventInput!
    }
    updateUser: { // args
      data: NexusGenInputs['updateUserInput']; // updateUserInput!
    }
  }
  Query: {
    events: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
      orderBy?: NexusGenInputs['EventOrderByWithRelationInput'][] | null; // [EventOrderByWithRelationInput!]
      where?: NexusGenInputs['EventWhereInput'] | null; // EventWhereInput
    }
    getEvent: { // args
      data: NexusGenInputs['getEventInput']; // getEventInput!
    }
    getEventStatistic: { // args
      data: NexusGenInputs['getEventStatisticInput']; // getEventStatisticInput!
    }
    getUser: { // args
      data: NexusGenInputs['getUserInput']; // getUserInput!
    }
    media: { // args
      after?: NexusGenInputs['MediaWhereUniqueInput'] | null; // MediaWhereUniqueInput
      before?: NexusGenInputs['MediaWhereUniqueInput'] | null; // MediaWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
      where?: NexusGenInputs['MediaWhereInput'] | null; // MediaWhereInput
    }
  }
  User: {
    archives: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    guest: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    maybego: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    myevent: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    participian: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
    planning: { // args
      after?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      before?: NexusGenInputs['EventWhereUniqueInput'] | null; // EventWhereUniqueInput
      first?: number | null; // Int
      last?: number | null; // Int
    }
  }
}

export interface NexusGenAbstractTypeMembers {
}

export interface NexusGenTypeInterfaces {
}

export type NexusGenObjectNames = keyof NexusGenObjects;

export type NexusGenInputNames = keyof NexusGenInputs;

export type NexusGenEnumNames = keyof NexusGenEnums;

export type NexusGenInterfaceNames = never;

export type NexusGenScalarNames = keyof NexusGenScalars;

export type NexusGenUnionNames = never;

export type NexusGenObjectsUsingAbstractStrategyIsTypeOf = never;

export type NexusGenAbstractsUsingStrategyResolveType = never;

export type NexusGenFeaturesConfig = {
  abstractTypeStrategies: {
    isTypeOf: false
    resolveType: true
    __typename: false
  }
}

export interface NexusGenTypes {
  context: any;
  inputTypes: NexusGenInputs;
  rootTypes: NexusGenRootTypes;
  inputTypeShapes: NexusGenInputs & NexusGenEnums & NexusGenScalars;
  argTypes: NexusGenArgTypes;
  fieldTypes: NexusGenFieldTypes;
  fieldTypeNames: NexusGenFieldTypeNames;
  allTypes: NexusGenAllTypes;
  typeInterfaces: NexusGenTypeInterfaces;
  objectNames: NexusGenObjectNames;
  inputNames: NexusGenInputNames;
  enumNames: NexusGenEnumNames;
  interfaceNames: NexusGenInterfaceNames;
  scalarNames: NexusGenScalarNames;
  unionNames: NexusGenUnionNames;
  allInputTypes: NexusGenTypes['inputNames'] | NexusGenTypes['enumNames'] | NexusGenTypes['scalarNames'];
  allOutputTypes: NexusGenTypes['objectNames'] | NexusGenTypes['enumNames'] | NexusGenTypes['unionNames'] | NexusGenTypes['interfaceNames'] | NexusGenTypes['scalarNames'];
  allNamedTypes: NexusGenTypes['allInputTypes'] | NexusGenTypes['allOutputTypes']
  abstractTypes: NexusGenTypes['interfaceNames'] | NexusGenTypes['unionNames'];
  abstractTypeMembers: NexusGenAbstractTypeMembers;
  objectsUsingAbstractStrategyIsTypeOf: NexusGenObjectsUsingAbstractStrategyIsTypeOf;
  abstractsUsingStrategyResolveType: NexusGenAbstractsUsingStrategyResolveType;
  features: NexusGenFeaturesConfig;
}


declare global {
  interface NexusGenPluginTypeConfig<TypeName extends string> {
  }
  interface NexusGenPluginInputTypeConfig<TypeName extends string> {
  }
  interface NexusGenPluginFieldConfig<TypeName extends string, FieldName extends string> {
  }
  interface NexusGenPluginInputFieldConfig<TypeName extends string, FieldName extends string> {
  }
  interface NexusGenPluginSchemaConfig {
  }
  interface NexusGenPluginArgConfig {
  }
}