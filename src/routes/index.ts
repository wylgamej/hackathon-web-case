import { Router } from 'express';
import inviteRoutes from './invite.route';

export default (app: { use: (arg0: string, arg1: Router) => void; }) => {
	app.use('', inviteRoutes);
};
