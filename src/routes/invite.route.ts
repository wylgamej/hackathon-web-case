/* eslint-disable no-magic-numbers */
import { Router } from 'express';
import { DataSourceConfig } from 'apollo-datasource';
import { BASE_URL } from '../config';
import prisma from '../prisma-client';
import { Context, InitialContext } from '../graphql/context';
import { UsersAPI } from '../datasources/users';

const router = Router();

router
	.get('/eventinvite', async (req, res) => {
		const context: InitialContext = {
			prisma,
			response: res,
			user: null,
		};

		const usersAPI = new UsersAPI();

		usersAPI.initialize({ context } as DataSourceConfig<Context>);

		const authorization = req?.headers?.authorization;

		const user = await usersAPI.findUserByToken(authorization);

		if (user) {
			const inviteLink = BASE_URL + req.url;
			const event = await prisma.event.findUnique({
				where: {
					inviteLink,
				},
			});

			await prisma.event.update({
				where: {
					id: event.id,
				},
				data: {
					members: {
						connect: {
							id: user.id,
						},
					},
				},
			});

			res.send('Success invite').status(200);
		} else {
			res.send('Error, User is not Authorized').status(401);
		}
	});

export default router;
